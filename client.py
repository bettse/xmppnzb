#!/usr/bin/env python

import logging
from optparse import OptionParser
from sleekxmpp.xmlstream import ET, tostring
from pubsub_events import PubsubEvents
import socket, ssl, requests, json, time, re
from datetime import datetime
import configparser
from xml.sax.saxutils import unescape
from sabnzbd import SABnzbd


_wildcard_to_regex = {
    '\\': r'\\',
    '^' : r'\^',
    '$' : r'\$',
    '.' : r'\.',
    '[' : r'\[',
    ']' : r'\]',
    '(' : r'\(',
    ')' : r'\)',
    '+' : r'\+',
    '?' : r'.' ,
    '|' : r'\|',
    '{' : r'\{',
    '}' : r'\}',
    '*' : r'.*'
}

class NzbEvents(PubsubEvents):
    def __init__(self):

        config = configparser.RawConfigParser()
        config.read('client.cfg')

        self.myCategories = config.get('client', 'categories').split(',')

        #SABnzbd related
        SABHOST = config.get('sabnzbd', 'host')
        SABPORT = config.get('sabnzbd', 'port')
        SABKEY = config.get('sabnzbd', 'key')
        self.sabnzbd = SABnzbd(SABHOST, int(SABPORT), SABKEY)
        self.last_import = 0
        self.accept_regex = []
        self.reject_regex = []

        #newznab key
        self.newznab = {}
        self.newznab['api_key'] = config.get('newznab', 'api_key')
        self.newznab['host'] = config.get('newznab', 'host')
        self.newznab['port'] = config.get('newznab', 'port')

        #XMPP variables
        self.pubsub_server = config.get('client', 'host')
        jid = config.get('client', 'jid')
        password = config.get('client', 'pass')
        super(NzbEvents, self).__init__(jid, password)


    def start(self, event):
        logging.info("Starting xmppnzb client %s" % (datetime.now()))
        self.get_roster()
        self.send_presence()

        #Subscribe to categories I'm not already following
        #Remove categories I'm no longer following
        subs = self.get_subscriptions()
        if subs and subs['pubsub'] and subs['pubsub']['subscriptions']:
          for sub in subs['pubsub']['subscriptions']:
              if(sub['node'] in self.myCategories):
                  self.myCategories.remove(sub['node'])
              else:
                  self.unsubscribe(sub['node'], sub['subid'])
        for cat in self.myCategories:
            self.subscribe(cat)

        self.import_regex()

    def import_regex(self):
        if (time.time() - self.last_import > 24*60*60):
            self.last_import = time.time()
            self.accept_regex = []
            self.reject_regex = []

            logging.info("Loading regex from sabnzbd api")
            #rss regex's
            rss = self.sabnzbd.get_config('rss')
            config = rss['config']
            details = config['rss'][0]
            for key in list(details.keys()):
                if "filter" in key:
                    filter = details[key]
                    mode = filter[3]
                    text = filter[4]
                    if text[:3].lower() == 're:':
                        txt = text[3:]
                    else:
                        txt = self.wildcard_to_re(text)
                    if mode == "A":
                        self.accept_regex.append(re.compile(txt, re.I))
                        logging.debug("Adding accept filter %s", txt)
                    elif mode == "R":
                        self.reject_regex.append(re.compile(txt, re.I))
                        logging.debug("Adding reject filter %s", txt)


    def wildcard_to_re(self, text):
        """ Convert plain wildcard string (with '*' and '?') to regex.
        """
        return ''.join([_wildcard_to_regex.get(ch, ch) for ch in text])

    def _publish(self, msg):
        """Handle receiving a publish item event."""
        logging.debug('Published item %s to %s:' % (
            msg['pubsub_event']['items']['item']['id'],
            msg['pubsub_event']['items']['node']))
        data = msg['pubsub_event']['items']['item']['payload']
        if data is not None:
            self.handleNZB(data, msg['pubsub_event']['items']['node'])
        else:
            logging.error('No item content')

    def subscribe(self, node):
        try:
            result = self['xep_0060'].subscribe(self.pubsub_server, node)
            logging.debug('Subscribed %s to node %s' % (self.boundjid.bare, node))
        except:
            logging.error('Could not subscribe %s to node %s' % (self.boundjid.bare, node))

    def unsubscribe(self, node, subid):
        try:
            result = self['xep_0060'].unsubscribe(self.pubsub_server, node, subid)
            logging.debug('Unsubscribed %s to node %s' % (self.boundjid.bare, node))
        except:
            logging.error('Could not unsubscribe %s to node %s' % (self.boundjid.bare, node))

    def get_subscriptions(self):
        result = ""
        try:
            result = self['xep_0060'].get_subscriptions(self.pubsub_server)
        except:
            logging.error('Could not get list of subscriptions')
        return result

    def test_ns_find(self, element, key):
      return unescape(element.find('test:%s' % key, {'test':'test'}).text)

    def handleNZB(self, xml, node):
        self.import_regex()#reload regexs every 24hr
        #expect list of [title, guid]
        try:
            title = self.test_ns_find(xml, 'name')
            guid = self.test_ns_find(xml, 'guid')
            url = "http://%s:%s/api?t=get&apikey=%s&guid=%s" % (self.newznab['host'], self.newznab['port'], self.newznab['api_key'], guid)
        except IndexError:
            logging.error("Couldn't parse %s" % xml)
            return
        #Look through accept_regex for at least one that matches
        matched = False
        for expression in self.accept_regex:
            if(expression.search(title)):
                logging.debug("Matched %s" % title)
                matched = True
        for expression in self.reject_regex:
            if(expression.search(title)):
                logging.debug("Rejecting %s" % title)
                matched = False
        if matched:
            sab_result = self.sabnzbd.addurl(url, title)
            logging.info("[+] %s at %s from %s" % (title, datetime.now(), node))
            logging.debug(sab_result)
        else:
            logging.info("[-] %s at %s from %s" % (title, datetime.now(), node))

if __name__ == '__main__':
    # Setup the command line arguments.
    optp = OptionParser()

    # Output verbosity options.
    optp.add_option('-q', '--quiet', help='set logging to ERROR',
                    action='store_const', dest='loglevel',
                    const=logging.ERROR, default=logging.INFO)
    optp.add_option('-i', '--info', help='set logging to INFO',
                    action='store_const', dest='loglevel',
                    const=logging.INFO, default=logging.INFO)
    optp.add_option('-d', '--debug', help='set logging to DEBUG',
                    action='store_const', dest='loglevel',
                    const=logging.DEBUG, default=logging.INFO)
    optp.add_option('-v', '--verbose', help='set logging to COMM',
                    action='store_const', dest='loglevel',
                    const=5, default=logging.INFO)

    opts, args = optp.parse_args()

    # Setup logging.
    logging.basicConfig(level=opts.loglevel, format='%(levelname)-8s %(message)s')


    xmpp = NzbEvents()
    # xmpp.ssl_version = ssl.PROTOCOL_SSLv3
    if xmpp.connect(use_tls=False):
        xmpp.process(block=True)
