#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sys
import logging
import time
import getpass
from optparse import OptionParser
import queue
import ssl
from xml.etree import cElementTree as ET
from pprint import pprint
import urllib.request, urllib.parse, urllib.error
import json
import sleekxmpp
from sleekxmpp.exceptions import IqError, IqTimeout
from nodes import node_list
import importlib

# Python versions before 3.0 do not use UTF-8 encoding
# by default. To ensure that Unicode is handled properly
# throughout SleekXMPP, we will set the default encoding
# ourselves to UTF-8.
if sys.version_info < (3, 0):
    importlib.reload(sys)
    sys.setdefaultencoding('utf8')

class EchoBot(sleekxmpp.ClientXMPP):

    """
    A simple SleekXMPP bot that will echo messages it
    receives, along with a short thank you message.
    """
    def __init__(self, jid, password):
        sleekxmpp.ClientXMPP.__init__(self, jid, password)

        # The session_start event will be triggered when
        # the bot establishes its connection with the server
        # and the XML streams are ready for use. We want to
        # listen for this event so that we we can intialize
        # our roster.
        self.add_event_handler("session_start", self.start)

        # The message event is triggered whenever a message
        # stanza is received. Be aware that that includes
        # MUC messages and error messages.
        self.add_event_handler("message", self.message)

        self.register_plugin('xep_0030') # Service Discovery
        self.register_plugin('xep_0004') # Data Forms
        self.register_plugin('xep_0060') # PubSub
        self.register_plugin('xep_0199') # self Ping


        self.add_handler("<message xmlns='jabber:client'><event xmlns='http://jabber.org/protocol/pubsub#event' /></message>", self.pubsubEventHandler, name='Pubsub Event', threaded=True)
        self.add_handler("<iq type='error' />", self.handleError, name='Iq Error')
        self.ps = self.plugin['xep_0060']
        self.events = queue.Queue()

        self.pshost = "pubsub." + self.boundjid.host
        self.add_event_handler("setup_nodes", self.setup_nodes, threaded=False)

    def pubsubEventHandler(self, xml):
        #print "Got a pubsub event"
        for item in xml.findall('{http://jabber.org/protocol/pubsub#event}event/{http://jabber.org/protocol/pubsub#event}items/{http://jabber.org/protocol/pubsub#event}item'):
            self.events.put(item.get('id', '__unknown__'))
        for item in xml.findall('{http://jabber.org/protocol/pubsub#event}event/{http://jabber.org/protocol/pubsub#event}items/{http://jabber.org/protocol/pubsub#event}retract'):
            self.events.put(item.get('id', '__unknown__'))
        for item in xml.findall('{http://jabber.org/protocol/pubsub#event}event/{http://jabber.org/protocol/pubsub#event}collection/{http://jabber.org/protocol/pubsub#event}disassociate'):
            self.events.put(item.get('node', '__unknown__'))
        for item in xml.findall('{http://jabber.org/protocol/pubsub#event}event/{http://jabber.org/protocol/pubsub#event}collection/{http://jabber.org/protocol/pubsub#event}associate'):
            self.events.put(item.get('node', '__unknown__'))

    def handleError(self, xml):
        error = xml.find('{jabber:client}error')
        if(error):
            self.lasterror =  error.getchildren()[0].tag.split('}')[-1]

    def start(self, event):
        """
        Process the session_start event.

        Typical actions for the session_start event are
        requesting the roster and broadcasting an intial
        presence stanza.

        Arguments:
            event -- An empty dictionary. The session_start
                     event does not provide any additional
                     data.
        """
        self.send_presence(pshow="online")
        self.get_roster()

    def message(self, msg):
        """
        Process incoming message stanzas. Be aware that this also
        includes MUC messages and error messages. It is usually
        a good idea to check the messages's type before processing
        or sending replies.

        Arguments:
            msg -- The received message stanza. See the documentation
                   for stanza objects and the Message stanza to see
                   how it may be used.
        """
        logging.debug(msg)
        msg.reply("Thanks for sending\n%(body)s" % msg).send()

    def setup_nodes(self, data):
        All = '0'
        others = [(str(x), str(y)) for x,y in node_list] #stringify
        toplevel = list(set([x for x,y in others]))
        #"All" node is the root and should be a collection of all X000 nodes.  All X000 nodes will contain their respective XY00 nodes

        try:
            logging.info("PubSub host is %s" % self.pshost)
            disco = self.ps.get_nodes(self.pshost)
            nodes = [node for jid,node,_ in disco['disco_items']['items']]
            logging.info("Nodes: %s" % nodes)

            if All not in nodes:
                self.ps.create_node(self.pshost, All)
                logging.info("'All' node created")

            disco = self.ps.get_nodes(self.pshost)
            nodes = [node for jid,node,_ in disco['disco_items']['items']]
            if All in nodes:
                for n in toplevel:
                    if n not in nodes:
                        self.ps.create_node(self.pshost, n)
                        logging.info("%s node created" % n)

            disco = self.ps.get_nodes(self.pshost)
            nodes = [node for jid,node,_ in disco['disco_items']['items']]
            for parent, child in others:
                logging.info("Considering %s : %s " % (parent, child))
                if parent in nodes and child not in nodes:
                    self.ps.create_node(self.pshost, child)
                    logging.info("%s node created" % child)

        except IqError as err:
            logging.error('There was an error getting the roster: %s' % (err.iq['error']['condition']))
            self.disconnect()
            exit()

if __name__ == '__main__':
    # Setup the command line arguments.
    optp = OptionParser()

    # Output verbosity options.
    optp.add_option('-q', '--quiet', help='set logging to ERROR',
                    action='store_const', dest='loglevel',
                    const=logging.ERROR, default=logging.INFO)
    optp.add_option('-d', '--debug', help='set logging to DEBUG',
                    action='store_const', dest='loglevel',
                    const=logging.DEBUG, default=logging.INFO)
    optp.add_option('-v', '--verbose', help='set logging to COMM',
                    action='store_const', dest='loglevel',
                    const=5, default=logging.INFO)

    # JID and password options.
    optp.add_option("-j", "--jid", dest="jid",
                    help="JID to use")
    optp.add_option("-p", "--password", dest="password",
                    help="password to use")

    opts, args = optp.parse_args()

    # Setup logging.
    logging.basicConfig(level=opts.loglevel, format='%(levelname)-8s %(message)s')

    if opts.jid is None:
        opts.jid = input("Username: ")
    if opts.password is None:
        opts.password = getpass.getpass("Password: ")

    xmpp = EchoBot(opts.jid, opts.password)

    # If you are working with an OpenFire server, you may need
    # to adjust the SSL version used:
    #xmpp.ssl_version = ssl.PROTOCOL_SSLv3

    # Connect to the XMPP server and start processing XMPP stanzas.
    if xmpp.connect():
        # If you do not have the pydns library installed, you will need
        # to manually specify the name of the server if it does not match
        # the one in the JID. For example, to use Google Talk you would
        # need to use:
        #
        # if xmpp.connect(('talk.google.com', 5222)):
        #     ...
        xmpp.process(threaded=True)
        logging.info("Sleeping 10s")
        time.sleep(10)
        data = None
        xmpp.event("setup_nodes", data)
        time.sleep(2*60)
        logging.info("Sleeping 2minutes")
    else:
        logging.warn("Unable to connect.")
