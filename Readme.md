XMPP NZB
====

Why poll when you can push?  Instead of using RSS to check for new NZBs against your favorite indexer, have them publish once to an xmpp server, and let it handle pushing (publish) the nzb to all interested (subscribe) parties.  The pubsub nodes are the newznab categories (ex 5040), and thus the client gets only the areas that interest them.  From there, they can check the new nzb against a list of regexes to determine if its worth downloading.

### Requirements:

Client:
 * SleekXMPP
 * Access to XMPP account on server
 * SABnzbd+ (the client.py gets the list of regexes from the first rss feed)

Server:
 * The development-postgres branch of [Pynab](https://github.com/Murodese/pynab)
 * xmpp server (I've used openfire and prosody)

Contact me if interested in testing the client against my xmpp server running pynab

